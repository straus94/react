import React from 'react';
import ReactDom from 'react-dom';
import HeaderBlock from "./Components/HeaderBlock";
import './index.css';

const AppList = () => {
    const items = ['Item 1', 'Item 2', 'Item 3', 'Item 4'];
    const firstItem = 'Item 0';

    const isAuth = true;

    return (
        <ul>
            { isAuth ? firstItem : null }
            { items.map(item => <li>{item}</li>) }
            <li>{ items[0] }</li>
            <li>{ items[1] }</li>
        </ul>
    );
}

const AppHeader = () => {
    return (
        <h1 className='header'>Hello World!!!</h1>
    );
}

const AppInput = () => {
    const placeholder = 'Type text...';

    return (
        <label htmlFor="search">
            <input id="search" type="text" placeholder={placeholder}/>
        </label>
    );
}

const App = () => {
    return (
        <>
            <HeaderBlock/>
            <AppHeader/>
            <AppList />
            <AppHeader/>
            <AppList />
        </>
    );
}

ReactDom.render(<App />, document.getElementById('root'))
