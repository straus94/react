import React from "react";
import s from './HeaderBlock.module.scss'
import { ReactComponent as ReactLogoSvg} from "../../logo.svg";

console.log(s);
const HeaderBlock = () => {

    return (
        <div className={s.cover}>
            <div className={s.wrap}>
                <h1 className={s.header}>Something header</h1>
                <ReactLogoSvg />
                <p className={s.decr}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet, natus.</p>
            </div>
        </div>
    );
}

export default HeaderBlock;
